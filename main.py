import os
from datetime import datetime
import csv
import pandas
import matplotlib.pyplot as plt
from lxml.html import fromstring


with open("report.xls") as f:
    html_text = f.read()

html = fromstring(html_text)
element = html.xpath('/html/body/table/tbody/tr')[1:]


user_id = 0
rows = dict()
time_order_rows = list()  # Для последнего необязательного задания
for tr in element:
    element_class = tr.attrib.get('class')
    if element_class == "group":
        user_id = tr.get("data-guid")
    else:
        order_id = tr.get("data-guid")
        data = list(tr)
        call_sign = data[0].text
        number = data[1].text
        order = data[2].text
        payment_type = data[5].text
        total = int(float(data[7].text.replace(",", ".")))
        driver_commission = float(data[10].text.replace(",", "."))
        working_conditions = data[13].text

        on_place = data[17].text
        on_place_datetime = datetime(year=2019, month=1, day=1, hour=int(on_place[:2]), minute=int(on_place[3:]))
        order_done = data[21].text
        order_done_datetime = datetime(year=2019, month=1, day=1, hour=int(order_done[:2]), minute=int(order_done[3:]))
        trip_time = order_done_datetime - on_place_datetime
        trip_time = trip_time.seconds // 60

        time_order_rows.append([total, trip_time])

        rows[order_id] = {
            "позывной": call_sign,
            "номер": number,
            "заказ": order,
            "способ оплаты": payment_type,
            "сумма": total,
            "комиссия с водителя": driver_commission,
            "условия работы": working_conditions,
            "id водителя": user_id
        }


with open("result.csv", mode="w", encoding="UTF-8") as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    first_row = [
        "id заказа",
        "позывной",
        "номер",
        "заказ",
        "способ оплаты",
        "сумма",
        "комиссия с водителя",
        "условия работы",
        "id водителя"
    ]
    writer.writerow(first_row)

    for key in rows.keys():
        new_row = [
            key,
            rows[key]["позывной"],
            rows[key]["номер"],
            rows[key]["заказ"],
            rows[key]["способ оплаты"],
            rows[key]["сумма"],
            rows[key]["комиссия с водителя"],
            rows[key]["условия работы"],
            rows[key]["id водителя"],
        ]
        writer.writerow(new_row)

df = pandas.read_csv("result.csv")  # загрузка данных
df = df[df["сумма"] > 0]  # Отфильтруйте заказы, оставив только те, которые имеют ненулевую сумму

cash_type = len(df[df["способ оплаты"] == "Безналичные"])  # Подсчитайте, сколько было наличных заказов
not_cash_type = len(df[df["способ оплаты"] == "Наличные"])  # а сколько — безналичных
print(f"Наличных заказов - {cash_type}\n"
      f"Безналичных заказов - {not_cash_type}\n")

df_sum = df.groupby(["id водителя"]).agg({"сумма": ["sum"], "комиссия с водителя": ["sum"]})

print("сумма и комиссии заказов для каждого водителя:")
print(df_sum)
total = df["сумма"]
commission = df["комиссия с водителя"]
order = df["заказ"]


def round_time(x):
    hour = x[9:-3]
    hour = "0" + hour if len(hour) == 1 else hour
    minute = int(x[-2:])
    delta_minute = minute % 5
    minute = str(minute - delta_minute) if minute > 9 else "0" + str(minute - delta_minute)
    return f"{hour}:{minute}"


order = order.apply(round_time)
order = order.sort_values()

graph_path = r'graph'
if not os.path.exists(graph_path):
    os.makedirs(graph_path)


def make_hist(x, title=None, ylabel=None, xlabel=None, save_path="hist", cumulative=False, density=None, htype="step"):
    plt.clf()
    plt.hist(x, cumulative=cumulative, density=density, histtype=htype, bins=100)
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.savefig(save_path)


make_hist(total, "Функция распределения суммы", "Вероятность", "Сумма", "graph/cdf_total", True, True)
make_hist(commission, "Функция распределения коммисии", "Вероятность", "Коммисия", "graph/cdf_commission", True, True)
make_hist(total, "Плотность вероятности суммы", "Вероятность", "Сумма", "graph/pdf_total", False, True)
make_hist(commission, "Плотность вероятности коммисии", "Вероятность", "Коммисия", "graph/pdf_commission", False, True)

plt.clf()
plt.rcParams.update({'font.size': 8})
plt.figure(figsize=(15, 10))
plt.autoscale(tight=False)
plt.tick_params(direction='inout', labelsize="large")
plt.xticks(range(0, len(order)//12, 12))
plt.hist(order, bins=100)
plt.title("Зависимость количества заказов от времена суток(с точностью в 5 мин)")
plt.ylabel("количество заказов")
plt.xlabel("Время суток")
plt.savefig("graph/order_for_time")

df_time = pandas.DataFrame(time_order_rows, columns=["Сума", "Время поездки(минут)"])
df_time = df_time[df_time["Сума"] > 0]
df_time = df_time.groupby(["Сума"]).mean()

plt.clf()
df_time.plot.line()
plt.xlabel("Сума")
plt.ylabel("Время поездки в минутах")
plt.savefig("graph/sum_forecast")
